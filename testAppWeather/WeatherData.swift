import Foundation

struct WeatherData: Decodable  {
    var main: Main
    var dt : Int
}

struct Main: Decodable {
    var temp: Double
}
