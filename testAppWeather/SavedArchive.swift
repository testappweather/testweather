import Foundation

import Foundation

class SavedArchive: Codable{
    
    var archiveCity = [String]()
    var archiveLat = [String]()
    var archiveLong = [String]()
    var archiveDate = [String]()
    var archiveTemp = [String]()
    
    init(archiveCity : [String], archiveLat : [String], archiveLong: [String], archiveDate: [String], archiveTemp: [String]){
        self.archiveCity = archiveCity
        self.archiveLat = archiveLat
        self.archiveLong = archiveLong
        self.archiveDate = archiveDate
        self.archiveTemp = archiveTemp
    }
    
    enum CodingKeys: String, CodingKey {
        case archiveCity, archiveLat, archiveLong, archiveDate, archiveTemp
    }

    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.archiveCity = try container.decode([String].self, forKey: .archiveCity)
        self.archiveLat = try container.decode([String].self, forKey: .archiveLat)
        self.archiveLong = try container.decode([String].self, forKey: .archiveLong)
        self.archiveDate = try container.decode([String].self, forKey: .archiveDate)
        self.archiveTemp = try container.decode([String].self, forKey: .archiveTemp)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(self.archiveCity, forKey: .archiveCity)
        try container.encode(self.archiveLat, forKey: .archiveLat)
        try container.encode(self.archiveLong, forKey: .archiveLong)
        try container.encode(self.archiveDate, forKey: .archiveDate)
        try container.encode(self.archiveTemp, forKey: .archiveTemp)
      }
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}
