//
//  ArchiveViewController.swift
//  testAppWeather
//
//  Created by Daniil Shlapak on 20.10.21.
//

import UIKit

class ArchiveViewController: UIViewController {

    @IBOutlet weak var mainTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainTableView.reloadData()
        self.mainTableView.tableFooterView = UIView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        mainTableView.reloadData()
    }
    
    private func openDetailsVC(){
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController else {
            return
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension ArchiveViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Manager.shared.archiveTemp.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ArchiveTableViewCell", for: indexPath) as? ArchiveTableViewCell else {return UITableViewCell()}
        cell.configure(city: Manager.shared.archiveCity[indexPath.row], date: Manager.shared.archiveDate[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Manager.shared.numberOfRow = indexPath.row
        self.openDetailsVC()
    }
}
