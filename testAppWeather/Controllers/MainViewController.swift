import UIKit
import CoreLocation

class MainViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var longLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var blur: UIVisualEffectView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    var address : String = ""
    var dateNow : String = "" {
        didSet {
            if dateNow != ""{
                self.blur.isHidden = true
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    let locationManager = CLLocationManager()
    let geocoder = CLGeocoder()
    let formatter = DateFormatter()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkInternet()
        getSavedData()
        addressLabel.numberOfLines = 0
    }
    
    private func checkInternet(){
        if NetworkMonitorManager.shared.isConnected {
            self.blur.isHidden = false
            self.activityIndicator.startAnimating()
            self.getUserLocation()
        }else{
            self.blur.isHidden = false
            present(Manager.shared.createAlert(title: "No internet connection", with: "Can't get weather", blur: blur), animated: true) {
                self.showwWeatherWithoutInternet()
            }
        }
    }
    
    private func showwWeatherWithoutInternet(){
        let index = Manager.shared.archiveCity.count - 1
        self.addressLabel.text = Manager.shared.archiveCity[index]
        self.latLabel.text = Manager.shared.archiveLat[index]
        self.longLabel.text = Manager.shared.archiveLong[index]
        self.dateLabel.text = Manager.shared.archiveDate[index]
        self.tempLabel.text = Manager.shared.archiveTemp[index]
    }
    
    private func getSavedData(){
        let savedData = UserDefaults.standard.value(SavedArchive.self, forKey: "savedArchive")
        guard let savedArchiveCity = savedData?.archiveCity else {return}
        guard let savedArchiveLat = savedData?.archiveLat else {return}
        guard let savedArchiveLong = savedData?.archiveLong else {return}
        guard let savedArchiveDate = savedData?.archiveDate else {return}
        guard let savedArchiveTemp = savedData?.archiveTemp else {return}

        Manager.shared.archiveCity = savedArchiveCity
        Manager.shared.archiveLat = savedArchiveLat
        Manager.shared.archiveLong = savedArchiveLong
        Manager.shared.archiveDate = savedArchiveDate
        Manager.shared.archiveTemp = savedArchiveTemp
    }
    
    private func getUserLocation(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    private func convertDate() -> String {
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        let endDate = formatter.string(from: Date())
        return endDate
    }
    
    private func processResponse(withPlacemarks placemarks: [CLPlacemark]?, error: Error?) {

        if let error = error {
            print("Unable to Reverse Geocode Location (\(error))")
            self.addressLabel.text = "Can't load address"

        } else {
            if let placemarks = placemarks, let placemark = placemarks.first {
                self.addressLabel.text = placemark.compactAddress
                self.address = placemark.compactAddress ?? ""
            } else {
               print("OOps")
            }
        }
    }
    
    @IBAction func updateButtonPressed(_ sender: UIButton) {
        checkInternet()
    }
}

extension MainViewController {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location : CLLocationCoordinate2D = manager.location!.coordinate
        let long = location.longitude
        let lat = location.latitude
        
        self.latLabel.text = "\(lat)"
        self.longLabel.text = "\(long)"
        geocoder.reverseGeocodeLocation(locations[0]) { placeMark, error in
            self.processResponse(withPlacemarks: placeMark, error: error)
        }

        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(long)&units=metric&appid=037e78fd5a7cac6085401d301af54e36") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data {
                do {
                    let weatherData : WeatherData?
                    weatherData = try JSONDecoder().decode(WeatherData.self, from: data)
                    
                    let temp = Int(weatherData?.main.temp ?? 0)
                    
                    DispatchQueue.main.async {
                        self.tempLabel.text = "\(temp)°C"
                        self.dateNow = self.convertDate()
                        self.dateLabel.text = self.dateNow
                        Manager.shared.archiveCity.append(self.address)
                        Manager.shared.archiveDate.append(self.dateNow)
                        Manager.shared.archiveLat.append("\(lat)")
                        Manager.shared.archiveLong.append("\(long)")
                        Manager.shared.archiveTemp.append("\(temp)°C")
                        let savedArchive = SavedArchive(archiveCity: Manager.shared.archiveCity, archiveLat: Manager.shared.archiveLat, archiveLong: Manager.shared.archiveLong, archiveDate: Manager.shared.archiveDate, archiveTemp: Manager.shared.archiveTemp)
                        UserDefaults.standard.set(encodable: savedArchive, forKey: "savedArchive")
                    }
                }catch {
                    print(error)
                }
            }
        }
        task.resume()
        locationManager.stopUpdatingLocation()
    }
}

extension CLPlacemark {

    var compactAddress: String? {
        if let name = name {
            var result = name
            if let street = thoroughfare {
                result += ", \(street)"
            }
            if let city = locality {
                result += ", \(city)"
            }
            if let country = country {
                result += ", \(country)"
            }
            return result
        }
        return nil
    }
}
