//
//  DetailsViewController.swift
//  testAppWeather
//
//  Created by Daniil Shlapak on 20.10.21.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var longLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showData()

    }
    
    func showData(){
        self.addressLabel.text = Manager.shared.archiveCity[Manager.shared.numberOfRow]
        self.latLabel.text = Manager.shared.archiveLat[Manager.shared.numberOfRow]
        self.longLabel.text = Manager.shared.archiveLong[Manager.shared.numberOfRow]
        self.tempLabel.text = Manager.shared.archiveTemp[Manager.shared.numberOfRow]
        self.dateLabel.text = Manager.shared.archiveDate[Manager.shared.numberOfRow]
    }
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
