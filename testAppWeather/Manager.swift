import Foundation
import UIKit

class Manager {
    
    static let shared = Manager()
    init(){}
    
    var archiveCity = [String]()
    var archiveLat = [String]()
    var archiveLong = [String]()
    var archiveDate = [String]()
    var archiveTemp = [String]()
    var numberOfRow : Int = 0
    
    func createAlert(title : String, with text : String, blur : UIVisualEffectView) -> UIAlertController{
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: { _ in
            blur.isHidden = true
        })
        let exitAction = UIAlertAction(title: "Exit", style: .cancel) { _ in
            exit(0)
        }
        alert.addAction(exitAction)
        alert.addAction(okAction)
        return alert
    }
}


